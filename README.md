# README #

Simple Folder View

### What is this repository for? ###

Egnyte

### How do I get set up? ###

Simply run
```
npm install
```

If you get some errors, try to build it using the next sequence:

```
npm install
bower install
gulp
```

### Technologies and Approaches ###
* Backbone.js (+ Backbone.LocalStorage for mock up data sync)
* RequireJS
* Gulp
* Jade
* Stylus
* Font Awesome

### Browser's support ###
* IE9+
* Chrome
* Safari

### Troubleshooting ###
For any IE you have to serve index.html file on any web server (IIS/Node/Apache). Keep in mind that IIS doesn't support woff2 MIME type by default.

### Possible improvements ###
* Compress font-awesome.css into the main.css build
* Test covering

### Author ###
Kirill Buga
