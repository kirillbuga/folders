require.config({
	baseUrl: 'js',
	paths: {
		jquery: "../bower_components/jquery/jquery.min",
		underscore: "../bower_components/underscore/underscore-min",
		backbone: "../bower_components/backbone/backbone",
		backboneLocalStorage: "../bower_components/backbone.localstorage/backbone.localStorage-min"
	},
	shim: {
		underscore: {
			exports: '_'
		},
		jquery: {
			exports: 'jQuery'
		},
		backbone: {
			deps: ['jquery', 'underscore'],
			exports: 'Backbone'
		},
		backboneLocalStorage: ['backbone']
	},
	deps: []
});

require(['backboneLocalStorage', 'views/appV'], function(localStorage, AppV) {
	'use strict';
	window.ENTER_KEY = 13;
	_.templateSettings = {
		evaluate: /\{\%(.+?)\}\}/g,
		interpolate: /\{\{(.+?)\}\}/g
	};
	return new AppV();
});