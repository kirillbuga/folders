define(function(require) {
	'use strict';

	var ItemM = require('models/itemM');
	var Backbone = require('backbone');
	var backboneLocalStorage = require('backboneLocalStorage');
	
	var ItemC = Backbone.Collection.extend({
		model: ItemM,
		localStorage: new Backbone.LocalStorage('folders'),
		comparator: 'type',
		initialize: function() {
			this.path = '';
			this.listenTo(Backbone, 'app:path', this.setFilter);
		},

		setFilter: function(path) {
			this.path = path || '';
		},

		filtered: function() {
			return this.where({path: this.path});
		},

		selected: function() {
			return this.where({selected: true});
		},

		getSubitems: function(path) {
			var regex = new RegExp('^' + path);
			return this.filter(function(item) {
				return regex.test(item.get('path'));
			});
		}
	});

	return new ItemC();
});