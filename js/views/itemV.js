define(function(require) {
	'use strict';

	var Types = require('enums/types');
	var Backbone = require('backbone');

	var ItemV = Backbone.View.extend({
		initialize: function() {
			var type = this.model.get('type');
			switch(type) {
				case Types.Folder:
					this.template = _.template($('#folder-template').html());
					break;
				case Types.File:
					this.template = _.template($('#file-template').html());
					break;
				case Types.None:
					this.template = _.template($('#item-stub-template').html());
					break;
				default:
					throw 'Unsupported item type';
			}
			this.bindListeners();
		},

		events: {
			'click .item-selected': 'toggleSelected',
			'click .save-button': 'saveChanges',
			'click .cancel-button': 'cancelChanges',
			'click .item-name': 'handleClick',
			'keyup .item-text': 'handleEnter'
		},

		bindListeners: function() {
			this.listenTo(this.model, 'change:editing', this.render);
			this.listenTo(this.model, 'change:name', this.render);
		},

		toggleSelected: function() {
			this.model.set('selected', !this.model.get('selected'));
			this.model.save();
		},

		saveChanges: function() {
			var newName = this.$el.find('.item-text').val();
			this.model.set('name', newName, {silent: true});
			this.model.set('editing', false);
			this.model.save();
		},

		cancelChanges: function() {
			this.$el.find('.item-text').val(this.model.get('name'));
			this.model.set('editing', false);
		},

		handleEnter: function(e) {
			if (e.which === ENTER_KEY) {
				this.saveChanges();
			}
		},

		handleClick: function() {
			if(this.model.get('type') === Types.File) {
				this.toggleSelected();
			} else {
				var href = this.$el.find('a').attr('href');
				Backbone.history.navigate(href, {trigger: true, replace: true});
			}
		},

		render: function() {
			// workaround in order to prevent rerendering on item saving
			if (typeof this.model.changed.id !== 'undefined') {
				return this;
			}
			this.setElement(this.template(this.model.toJSON()));
			this.$el.toggleClass('editing', this.model.get('editing'));
			return this;
		}
	});

	return ItemV;
});