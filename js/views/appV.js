var app = app || {};

define(function(require) {
	'use strict';

	var Backbone = require('backbone');
	var _ = require('underscore');
	var ItemM = require('models/itemM');
	var ItemV = require('views/itemV');
	var Operations = require('enums/operations');
	var Types = require('enums/types');
	var Items = require('collections/itemC');
	var Router = require('routes/router');

	var AppView = Backbone.View.extend({
		el: '#root-container',
		initialize: function() {
			this.currentPath = '';
			var popup = $('#popup-hover');
			this.$ui = {
				popup:           popup,
				popupYes:        popup.find('#popup-yes'),
				popupNo:         popup.find('#popup-no'),
				selectAll:       this.$el.find('#select-all'),
				renameButton:    this.$el.find('#rename-button'),
				deleteButton:    this.$el.find('#delete-button'),
				itemsList:       this.$el.find('#items-list'),
				changeField:     this.$el.find('#new-rename-section'),
				changeText:      this.$el.find('#new-rename-field')
			};
			this.bindListeners();
			Items.fetch({reset: true});
			this.checkSelected();
		},

		events: {
			'click #new-file-button'   : 'newFile',
			'click #new-folder-button' : 'newFolder',
			'click #new-rename-ok'     : 'submit',
			'click #rename-button'     : 'rename',
			'click #delete-button'     : 'deteleItems',
			'click #new-rename-cancel' : 'editFinished',
			'click #select-all'        : 'toggleAllSelected'
		},

		bindListeners: function() {
			this.listenTo(Items, 'add', this.addOne);
			this.listenTo(Items, 'reset all', this.render);
			this.listenTo(Items, 'change:selected', this.checkSelected);
			this.listenTo(Backbone, 'app:path', this.setCurrentPath);
			this.bindUIElements();
		},

		bindUIElements: function() {
			var self = this;

			this.$ui.changeText.keyup(function(e) {
				if (e.which === ENTER_KEY) {
					self.submit();
				}
			});

			this.$ui.popupNo.click(function() {
				self.$ui.popup.hide();
			});

			this.$ui.popupYes.click(function() {
				self.deleteConfirmed();
				self.$ui.popup.hide();
			});
		},

		addOne: function (todo) {
			var view = new ItemV({ model: todo });
			this.$ui.itemsList.append(view.render().el);
		},

		render: function() {
			this.$ui.itemsList.html('');
			this.addOne(this.createPrevModel());
			_.each(Items.filtered(), this.addOne, this);
		},

		createPrevModel: function() {
			var path = this.currentPath.slice(0, this.currentPath.lastIndexOf('/'));
			return new ItemM({
				type: Types.None,
				link: path,
				name: '../'
			});
		},

		newFile: function() {
			this.lastOperation = Operations.NewFile;
			this.$ui.changeField.show();
		},

		newFolder: function() {
			this.lastOperation = Operations.NewFolder;
			this.$ui.changeField.show();
		},

		rename: function() {
			_.each(Items.selected(), function(item) {
				item.set('editing', true);
			});
		},

		submit: function() {
			Items.create(this.newAttributes());
			this.editFinished();
		},

		deteleItems: function() {
			if(Items.selected().length !== 0) {
				this.$ui.popup.show();
			}
		},

		deleteConfirmed: function() {
			_.each(Items.selected(), function(item) {
				if(item.get('type') === Types.Folder) {
					_.invoke(Items.getSubitems(item.get('link')), 'destroy');
				}
				item.destroy();
			});
			this.checkSelected();
		},

		checkSelected: function() {
			if(Items.selected().length === 0) {
				this.$ui.renameButton.toggleClass('disabled', true);
				this.$ui.deleteButton.toggleClass('disabled', true);				
			} else {
				this.$ui.renameButton.toggleClass('disabled', false);
				this.$ui.deleteButton.toggleClass('disabled', false);
			}
		},

		setCurrentPath: function(path) {
			this.toggleAllSelected(false);
			this.currentPath = path || '';
			this.render();
		},

		editFinished: function() {
			this.$ui.changeText.val('');
			this.lastOperation = '';
			this.$ui.changeField.hide();
		},

		toggleAllSelected: function(toBeSelected) {
			var forced = typeof toBeSelected === 'boolean' && toBeSelected === true;
			var selected =  forced || this.$ui.selectAll.is(':checked');
			Items.each(function (item) {
				item.set('selected', selected);
			});
		},

		newAttributes: function() {
			return {
				name: this.$ui.changeText.val(),
				path: this.currentPath,
				type: this.lastOperation === Operations.NewFile ? Types.File : Types.Folder
			};
		}
	});

	return AppView;
});