define(function(require) {
	'use strict';

	return {
		Folder: 'folder',
		File: 'file',
		None: 'none'
	};
});