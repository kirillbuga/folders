define(function(require) {
	'use strict';

	return {
		NewFile: 'new file',
		NewFolder: 'new folder',
		Update: 'update'
	};
});