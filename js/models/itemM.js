define(function(require) {
	'use strict';

	var Types = require('enums/types');
	var Backbone = require('backbone');
	
	var ItemM = Backbone.Model.extend({
		defaults: {
			type: Types.File,
			name: 'New Item',
			selected: false,
			editing: false,
			path: '',
			createdAt: new Date()
		},

		initialize: function() {
			var type = this.get('type');
			if(type === Types.Folder) {
				var link = this.get('path') + '/' + this.get('name');
				this.set('link', link);
			}
		}
	});

	return ItemM;
});