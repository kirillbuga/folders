define(function(require) {
	'use strict';
	var Backbone = require('backbone');

	var Router = Backbone.Router.extend({
		routes: {
			'*folder': 'setFolderPath'
		},

		setFolderPath: function (params) {
			Backbone.trigger('app:path', params);
		}
	});

	new Router();
	Backbone.history.start();
});