var nib       = require("nib");
var gulp      = require("gulp");
var jade      = require("gulp-jade");
var	stylus    = require("gulp-stylus");
var	jshint    = require("gulp-jshint");
var concat    = require("gulp-concat");
var plumber   = require("gulp-plumber");
var stylish   = require("jshint-stylish");
var Q         = require('q');
var rjs       = require('requirejs');

var paths = {
	js:        ["js/**/*.js"],
	styl:      ["styl/*.styl"],
	jade:      ["index.jade"],
	templates: ["templates/**/*.jade"]
};

var onError = function(args){
	console.log("============================= ERROR ===============================");
	console.log(args);
};

gulp.task('requirejs', function() {
	var deferred = Q.defer();
	rjs.optimize({
		appDir: 'js',
		baseUrl: ".",
		mainConfigFile: 'js/main.js',
		optimize: 'uglify',
		generateSourceMaps: false,
		preserveLicenseComments: false,
		useStrict: true,
		removeCombined: false,
		wrapShim: true,
		findNestedDependencies: true,
		dir: 'app_builds',
		modules: [
			{
				name: 'main'
			}
		]
	}, function(buildResponse) {
		deferred.resolve();
		return console.log('build response', buildResponse);
	})

	return deferred.promise;
});

gulp.task('jade', function() {
	gulp.src(paths.jade)
		.pipe(jade())
		.pipe(gulp.dest('./'));
});


gulp.task("scripts", function() {
	gulp.src(paths.js)
		.pipe(plumber({ errorHandler: onError }))
		.pipe(jshint())
		.pipe(jshint.reporter(stylish))
		.pipe(plumber.stop());
});

gulp.task("styles", function () {
	gulp.src(paths.styl)
		.pipe(plumber({ errorHandler: onError }))
		.pipe(stylus({ use: nib(), compress: true }))
		.pipe(concat("main.min.css"))
		.pipe(gulp.dest("css/"))
		.pipe(plumber.stop());
});

gulp.task("watch", function () {
	gulp.run('default');
	gulp.watch(paths.js, ["scripts"]);
	gulp.watch(paths.jade, ["jade"]);
	gulp.watch(paths.styl, ["styles"]);
});

gulp.task("default", ["styles", "scripts", "jade", "requirejs"]);